<?php

namespace WorkMote\IsCli\Shortcut;

use WorkMote\IsCli\IsCli;

/**
 * Shortcut function to run the same check.
 */
function isCli(): bool
{
    return IsCli::check();
}
